if (WIN32)
  find_program(DEPOTTOOLS_GCLIENT_EXECUTABLE
               NAMES gclient.bat
               PATHS ${PROJECT_SOURCE_DIR}/depot_tools)
  find_program(DEPOTTOOLS_GN_EXECUTABLE
               NAMES gn.bat
               PATHS ${PROJECT_SOURCE_DIR}/depot_tools)
  find_program(DEPOTTOOLS_NINJA_EXECUTABLE
               NAMES ninja.exe
               PATHS ${PROJECT_SOURCE_DIR}/depot_tools)
else (WIN32)
  find_program(DEPOTTOOLS_GCLIENT_EXECUTABLE
               NAMES gclient
               PATHS ${PROJECT_SOURCE_DIR}/depot_tools)
  find_program(DEPOTTOOLS_GN_EXECUTABLE
               NAMES gn
               PATHS ${PROJECT_SOURCE_DIR}/depot_tools)
  find_program(DEPOTTOOLS_NINJA_EXECUTABLE
               NAMES ninja
               PATHS ${PROJECT_SOURCE_DIR}/depot_tools)
endif (WIN32)

find_path(DEPOTTOOLS_DIR NAMES depot_tools PATHS ${PROJECT_SOURCE_DIR})
set(DEPOTTOOLS_DIR ${DEPOTTOOLS_DIR}/depot_tools)

include(${CMAKE_ROOT}/Modules/FindPackageHandleStandardArgs.cmake)
find_package_handle_standard_args(DepotTools
                                  REQUIRED_VARS DEPOTTOOLS_GCLIENT_EXECUTABLE DEPOTTOOLS_GN_EXECUTABLE DEPOTTOOLS_NINJA_EXECUTABLE DEPOTTOOLS_DIR
                                  FAIL_MESSAGE "Could not find the depot_tools executable.")
